﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LookController : MonoBehaviour {
    //Mouse sensitivity. Change this as needed.
    public float mouseSensitivity = 100f;
    //A reference to the player's transform
    public Transform playerBody;
    //A variable we use to store the relative rotation value of the mouse. This allows us to clamp the up/down movement.
    private float yRotation;

    //Determine whether or not the cursor is locked.
    //Disable to free the mouse.
    public bool cursorLocked;
    private void Start() {
        if (cursorLocked) {
            //Lock the cursor to the center of the screen.
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
    public void OnLook(InputValue value) {

    }
    private void Update() {
        //Get the mouse movements.
        float mouseX = (Input.GetAxis("Mouse X") * mouseSensitivity) * Time.deltaTime;
        float mouseY = (Input.GetAxis("Mouse Y") * mouseSensitivity) * Time.deltaTime;

        //Apply the mouse movement to the yRotation.
        yRotation -= mouseY;
        //Clamp Y Rotation between -90 and 90 degrees.
        yRotation = Mathf.Clamp(yRotation, -90f, 90f);
        //Rotate the camera up/down
        transform.localRotation = Quaternion.Euler(yRotation, 0f, 0f);
            //Rotate the player left/right
        playerBody.Rotate(Vector3.up * mouseX);

    }
}
